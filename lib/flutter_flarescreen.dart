import 'package:flutter/material.dart';

class FlutterFlareScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height/ 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 60.0, left:20.0, right:  20.0),
                    child: Text(
                      "This is flare button in question, click to see what it does. However, due to some reason, this is not even showing up in release. Please try to sign with your creds and check.",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_flare/main_screen.dart';
import 'package:flutter_flare/problem_screen.dart';
import 'package:flutter_flare/zoom_scaffold.dart';
import 'menu_screen.dart';



void main() => runApp(FlutterFlare());

class FlutterFlare extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FlutterFlare',
      theme: ThemeData(
          primarySwatch: Colors.grey,
          textTheme: TextTheme(
              body1: TextStyle(fontSize: 25.0),
              headline: TextStyle(fontSize: 25.0),
              title: TextStyle(fontSize: 30.0)),
          pageTransitionsTheme: PageTransitionsTheme(builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          })),
      home:
          //SplashScreen(),
          MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final menu = Menu(
    items: [
      MenuItem(
        id: 'option1',
        title: 'Issue',
      ),
      MenuItem(
        id: 'option2',
        title: 'Problem Statement',
      ),
    ],
  );

  var selectedMenuItemId = 'option1';
  var activeScreen = flareScreen;

  @override
  Widget build(BuildContext context) {
    //Setting the oreintation to portrait only --abheedevta

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    //hiding the statusbar
    SystemChrome.setEnabledSystemUIOverlays([]);

    return new ZoomScaffold(
      menuScreen: MenuScreen(
        menu: menu,
        selectedItemId: selectedMenuItemId,
        onMenuItemSelected: (String itemId) {
          selectedMenuItemId = itemId;

          if (itemId == 'option1') {
            setState(() => activeScreen = flareScreen);
          } else {
            setState(() => activeScreen = problemScreen);
          }
          if (itemId == 'option2') {
            setState(() => activeScreen = problemScreen);
          }
        },
      ),
      contentScreen: activeScreen,
    );
  }
}

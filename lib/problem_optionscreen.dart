import 'package:flutter/material.dart';

class ProblemFlareScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Material(
              color: Colors.black45,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 80.0,
                  ),
                  Expanded(
                    child: Container(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height / 2,
                              left: 10.0,
                              right: 10.0,
                              bottom: 10.0),
                          child: Container(
                            child: Text(
                              '''Hello 2dimensions team,

As posted on medium, here are the issue and details:
___________________________________
Hello Umberto,

Glad to be using Flare,

However, I have a menu animation created in flutter app.

It works perfectly in debug mode while doesn’t work when I am doing release.

It has a “menu state ” which is “menu icon” when it is idle, onTap, it opens the drawer and changes to “red close icon” and doing the second tap reverses

the things, the drawer closes and icon changes to “menu” again.

The problem is the icon does not render in release mode, it is there but doesn’t show up.

here is the code snippet for it :
    
    GestureDetector(
      onTap: () {
        menuController.toggle();
        },
          child: Center(
            child: Padding(
              padding: const 
              EdgeInsets.symmetric(
                  horizontal: 15.0),
              child: Container( 
                  FlareActor(
                    "assets/menu_close.flr",
                     alignment: 
                     Alignment.center,
                     fit: BoxFit.contain,
                     animation: animationName,
                     ),
                    ),
                  ),
                ),
            ),
Flare Actor wrapped in Container and Gesture detector for onTap
_____________________________________

  open() {
    _animationController.forward();
    animationName = "close";
    buttonController.forward();
    
  }
Animation Controller defines the Menu and Close buttons


1. This can bee seen in action when it is working with debug mode:
https://gph.is/g/ZrVQOxZ

2. And when it is not working with release mode:
https://gph.is/g/a9gzg54

Please help !

____________________________________

Q: Which device/os/version you’re running this on 
A: I have a loads of real devices. I checked on Apple iPhone 7(Running iOS 12.3), iPhone X(Running iOS 12.3.1), iPhone Xs Max(Running iOS 12.3.1), iPad 2(Running iOS 9.3.5), iPad Air(Running iOS 11). For Android i have gone with Google Pixel 3(Running Android Q Beta 2 and 3), Google Pixel 3(Running Android Pie May Update stable), OnePlus 6(OxygenOS based on Android™ Oreo stable), OnePlus 6T(OxygenOS based on Android Pie stable), Xiaomi Mi Max(MIUI Global 9.2.28 Beta based on Android 7.0), LG G6(Android 8.1.0), LG Q6(Android 8.1.0), LYF Water 7(Android 5.1).

Q: Which Flutter version and channel as well as the Flare-Flutter version and branch you’re currently using: 
A: Flutter version: Channel stable, v1.5.4-hotfix.2, on Mac OS X 10.14.4 18E226, locale en-GB
     Flare Flutter version: flare_flutter: ^1.5.0

The Flare file and the source code you’re running
Flare file is hosted @: https://www.2dimensions.com/a/abheedevta/files/flare/menu_close
Flutter code is hosted @ : https://gitlab.com/inlabz/flutter_problem.git


Please have a look

regards,
Abhishek''',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_flare/problem_optionscreen.dart';
import 'package:flutter_flare/zoom_scaffold.dart';

final Screen problemScreen = Screen(
    title: 'FlutterFlare',
    background: DecorationImage(
      fit: BoxFit.fitHeight, 
      image: AssetImage('assets/bg_ss.png'),
    ),
    contentBuilder: (BuildContext context) {
     SystemChrome.setEnabledSystemUIOverlays([]);
      return ProblemFlareScreen();
    }
);

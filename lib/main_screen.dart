import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_flare/flutter_flarescreen.dart';
import 'package:flutter_flare/zoom_scaffold.dart';

final Screen flareScreen = Screen(
    title: 'FlutterFlare',
    background: DecorationImage(
      fit: BoxFit.fitHeight, 
      image: AssetImage('assets/bg_ss.png'),
    ),
    contentBuilder: (BuildContext context) {
     SystemChrome.setEnabledSystemUIOverlays([]);
      return FlutterFlareScreen();
    }
);
